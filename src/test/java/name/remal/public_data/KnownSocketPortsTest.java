package name.remal.public_data;

import lombok.SneakyThrows;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static name.remal.public_data.KnownSocketPorts.isKnownTCPPort;
import static name.remal.public_data.KnownSocketPorts.isKnownUDPPort;
import static org.junit.Assert.fail;

public class KnownSocketPortsTest {

    @SneakyThrows
    private static int[] getPorts(String fieldName) {
        Field field = KnownSocketPorts.class.getDeclaredField(fieldName);
        field.setAccessible(true);
        return (int[]) field.get(null);
    }

    @Test
    public void portsAreUnique() {
        Stream.of("KNOWN_TCP_PORTS", "KNOWN_UDP_PORTS")
            .map(KnownSocketPortsTest::getPorts)
            .forEach(ports -> {
                Set<Integer> set = new HashSet<>();
                for (int port : ports) {
                    if (!set.add(port)) {
                        fail(format("Port %d is not unique", port));
                    }
                }
            });
    }

    @Test
    public void wellKnownTCPPortsArePresented() {
        asList(0, 22, 80).forEach(port -> {
            if (!isKnownTCPPort(port)) {
                fail(format("TCP port %d is not presented", port));
            }
        });
    }

    @Test
    public void wellKnownUDPPortsArePresented() {
        asList(0, 22).forEach(port -> {
            if (!isKnownUDPPort(port)) {
                fail(format("UDP port %d is not presented", port));
            }
        });
    }

}
